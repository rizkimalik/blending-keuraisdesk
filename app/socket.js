'use strict'
const fs = require('fs');
const axios = require('axios');
const logger = require('../helper/logger');
const { API_URL } = require('./config');
const https = require('https');

const httpsAgent = new https.Agent({
    rejectUnauthorized: false, // (NOTE: this will disable client verification)
    cert: fs.readFileSync('ssl/certificate.crt', 'utf8'),
    key: fs.readFileSync('ssl/private.key', 'utf8'),
});

const onSendEmailIn = async function ({ email }) {
    try {
        const data = email;
        data.channel = 'Email';
        data.name = email.efrom;
        data.message = email.esubject;

        const options = {
            url: `${API_URL}/blending/send_email_in`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_email_in', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_email_in', JSON.stringify(response.data));
            })
            .catch(function (error) {
                logger('socket/send_email_in', error.message);
            });
    }
    catch (error) {
        logger('socket/send_email_in', error.message);
    }
}

const onSendMsgAgent = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.uuid_agent = user.uuid;
        data.agent_handle = user.username;
        data.uuid_customer = customer.uuid;
        data.message = `Hi.. ${customer.name}, Ready to chat with ${user.username}`;

        const options = {
            url: `${API_URL}/blending/send_message_agent`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_message_agent', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_message_agent', JSON.stringify(response));
            })
            .catch(function (error) {
                logger('socket/send_message_agent', error.message);
            });
    }
    catch (error) {
        logger('socket/send_message_agent', error.message);
    }
}

const onSendMsgCustomer = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.agent_handle = user.username;
        data.uuid_agent = user.uuid;
        data.uuid_customer = customer.uuid;

        const options = {
            url: `${API_URL}/blending/send_message_cust`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_message_cust', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_message_cust', JSON.stringify(response));
            })
            .catch(function (error) {
                logger('socket/send_message_cust', error.message);
            });
    }
    catch (error) {
        logger('socket/send_message_cust', error.message);
    }
}

const onQueingChat = async function ({ data_customer, data_chat }) {
    try {
        const data = data_chat;
        data.uuid_customer = data_customer.uuid;
        data.message = `Kamu dalam antrian ke : ${data_chat.queing} silahkan tunggu beberapa saat.`;

        const options = {
            url: `${API_URL}/blending/queing_chat`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/queing_chat', options);

        await axios(options)
            .then(function (response) {
                logger('socket/queing_chat', JSON.stringify(response));
            })
            .catch(function (error) {
                logger('socket/queing_chat', error.message);
            });
    }
    catch (error) {
        logger('socket/queing_chat', error.message);
    }
}

const onSendMessageWhatsapp = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.uuid_agent = user.uuid;
        data.agent_handle = user.username;
        data.uuid_customer = customer.uuid;
        data.customer_name = customer.name;

        const options = {
            url: `${API_URL}/blending/send_message_whatsapp`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_message_whatsapp', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_message_whatsapp', JSON.stringify(response.data));
            })
            .catch(function (error) {
                logger('socket/send_message_whatsapp', JSON.stringify(error.message));
            });
    }
    catch (error) {
        logger('socket/send_message_whatsapp', error.message);
    }
}

const onSendTwitterDirectMessage = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.uuid_agent = user.uuid;
        data.agent_handle = user.username;
        data.uuid_customer = customer.uuid;
        data.customer_name = customer.name;

        const options = {
            url: `${API_URL}/blending/send_directmessage_twitter`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_directmessage_twitter', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_directmessage_twitter', JSON.stringify(response.data));
            })
            .catch(function (error) {
                logger('socket/send_directmessage_twitter', error.message);
            });
    }
    catch (error) {
        logger('socket/send_directmessage_twitter', error.message);
    }
}

const onSendFacebookMessenger = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.uuid_agent = user.uuid;
        data.agent_handle = user.username;
        data.uuid_customer = customer.uuid;
        data.customer_name = customer.name;

        const options = {
            url: `${API_URL}/blending/send_facebook_messenger`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_facebook_messenger', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_facebook_messenger', JSON.stringify(response.data));
            })
            .catch(function (error) {
                logger('socket/send_facebook_messenger', error.message);
            });
    }
    catch (error) {
        logger('socket/send_facebook_messenger', error.message);
    }
}

const onSendFacebookFeed = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.agent_handle = user.username;
        data.customer_name = customer.name;

        const options = {
            url: `${API_URL}/blending/send_facebook_feed`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_facebook_feed', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_facebook_feed', JSON.stringify(response.data));
            })
            .catch(function (error) {
                logger('socket/send_facebook_feed', error.message);
            });
    }
    catch (error) {
        logger('socket/send_facebook_feed', error.message);
    }
}

const onSendInstagramMessenger = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.uuid_agent = user.uuid;
        data.agent_handle = user.username;
        data.uuid_customer = customer.uuid;
        data.customer_name = customer.name;

        const options = {
            url: `${API_URL}/blending/send_instagram_messenger`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_instagram_messenger', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_instagram_messenger', JSON.stringify(response.data));
            })
            .catch(function (error) {
                logger('socket/send_instagram_messenger', error.message);
            });
    }
    catch (error) {
        logger('socket/send_instagram_messenger', error.message);
    }
}

const onSendInstagramFeed = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.agent_handle = user.username;
        data.customer_name = customer.name;

        const options = {
            url: `${API_URL}/blending/send_instagram_feed`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_instagram_feed', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_instagram_feed', JSON.stringify(response.data));
            })
            .catch(function (error) {
                logger('socket/send_instagram_feed', error.message);
            });
    }
    catch (error) {
        logger('socket/send_instagram_feed', error.message);
    }
}

const onSendMessageTelegram = async function ({ customer, user, chat }) {
    try {
        const data = chat;
        data.uuid_agent = user.uuid;
        data.agent_handle = user.username;
        data.uuid_customer = customer.uuid;
        data.customer_name = customer.name;

        const options = {
            url: `${API_URL}/blending/send_message_telegram`,
            httpsAgent,
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(data)
        }
        logger('socket/send_message_telegram', options);

        await axios(options)
            .then(function (response) {
                logger('socket/send_message_telegram', JSON.stringify(response.data));
            })
            .catch(function (error) {
                logger('socket/send_message_telegram', error.message);
            });
    }
    catch (error) {
        logger('socket/send_message_telegram', error.message);
    }
}

module.exports = {
    onSendMsgAgent,
    onSendMsgCustomer,
    onSendEmailIn,
    onQueingChat,
    onSendMessageWhatsapp,
    onSendTwitterDirectMessage,
    onSendFacebookMessenger,
    onSendFacebookFeed,
    onSendInstagramMessenger,
    onSendInstagramFeed,
    onSendMessageTelegram,
}

